
#include "renderjournal.h"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <optional>
#include <print>
#include <ranges>
#include <utility>
#include <vector>
#include <unordered_map>

using namespace std::chrono_literals;

std::vector<std::string> split(std::string_view line)
{
    auto split = std::ranges::split_view(line, ',');
    std::vector<std::string> ret;
    for (const auto &val : split) {
        ret.emplace_back(val.begin(), val.end());
    }
    return ret;
}

std::optional<int64_t> saneToInt(const std::string &value)
{
    try {
        return std::stol(value);
    } catch (std::exception &e) {
        std::println("failure with component '{}': {}", value, e.what());
    }
    return std::nullopt;
}

void analyze(std::filesystem::path file)
{
    const std::vector<std::string> expectedNames{
        "target pageflip timestamp",
        "pageflip timestamp",
        "render start",
        "render end",
        "safety margin",
        "refresh duration",
        "vrr",
        "tearing",
    };

    struct FrameStats {
        std::chrono::nanoseconds targetPageflipTimestamp;
        std::chrono::nanoseconds actualPageflipTimestamp;
        std::chrono::nanoseconds renderStart;
        std::chrono::nanoseconds renderEnd;
        std::chrono::nanoseconds renderTimeEstimation;
        std::chrono::nanoseconds safetyMargin;
        std::chrono::nanoseconds refreshDuration;
        bool vrr;
        bool tearing;
    };
    std::vector<FrameStats> frames;

    std::fstream in(file, std::ios::in);
    std::string line;
    // parse the names
    getline(in, line);

    std::vector<std::string> names = split(line);
    const bool allPresent = std::ranges::all_of(expectedNames, [&names](const auto &string) {
        return std::ranges::contains(names, string);
    });
    if (!allPresent) {
        std::println("some value is missing from the list");
        return;
    }
    const bool hasRenderTimeEstimation = std::ranges::contains(names, "predicted render time");

    // and also the first value that's unusable
    getline(in, line);
    while (getline(in, line)) {
        auto split = std::ranges::split_view(line, ',');
        std::unordered_map<std::string, int64_t> values;
        uint32_t index = 0;
        for (const auto &val : split) {
            if (index >= names.size()) {
                values.clear();
                break;
            }
            if (auto parsed = saneToInt(std::string(val.begin(), val.end()))) {
                values[names[index]] = *parsed;
            } else {
                std::println("parsing a value failed in line '{}'", line);
                break;
            }
            index++;
        }
        if (values.size() != names.size()) {
            std::println("Wrong amount of values in line '{}'", line);
            continue;
        }
        frames.push_back(FrameStats{
            .targetPageflipTimestamp = std::chrono::nanoseconds(values["target pageflip timestamp"]),
            .actualPageflipTimestamp = std::chrono::nanoseconds(values["pageflip timestamp"]),
            .renderStart = std::chrono::nanoseconds(values["render start"]),
            .renderEnd = std::chrono::nanoseconds(values["render end"]),
            .renderTimeEstimation = std::chrono::nanoseconds(values["predicted render time"]),
            .safetyMargin = std::chrono::nanoseconds(values["safety margin"]),
            .refreshDuration = std::chrono::nanoseconds(values["refresh duration"]),
            .vrr = values["vrr"] != 0,
            .tearing = values["tearing"] != 0,
        });
    }

    std::chrono::nanoseconds totalRenderTime{0};
    std::chrono::nanoseconds unnecessaryLatency{0};
    int dropped = 0;
    int cpuDropped = 0;
    int lateStartDropped = 0;
    int gpuDropped = 0;
    int estimatedTooLittle = 0;
    int estimatedTooMuch = 0;
    KWin::RenderJournal journal;
    for (const auto &stats : frames) {
        const std::optional<std::chrono::nanoseconds> renderTime = stats.renderEnd != 0ns && stats.renderStart != 0ns ? std::make_optional(stats.renderEnd - stats.renderStart) : std::nullopt;
        if (stats.actualPageflipTimestamp > stats.targetPageflipTimestamp + stats.refreshDuration / 2 && !stats.vrr) {
            dropped++;
            if (stats.renderEnd != 0ns && stats.renderEnd > stats.targetPageflipTimestamp - stats.safetyMargin) {
                if (hasRenderTimeEstimation && renderTime && stats.renderTimeEstimation >= *renderTime && stats.renderStart > stats.targetPageflipTimestamp - stats.safetyMargin - *renderTime) {
                    lateStartDropped++;
                } else {
                    gpuDropped++;
                }
            } else {
                cpuDropped++;
            }
        }
        if (renderTime) {
            if (journal.result() < *renderTime) {
                estimatedTooLittle++;
            } else if (journal.result() > *renderTime * 1.5) {
                estimatedTooMuch++;
            }
            // render loop uses 1ms of additional safety
            // schedulers aren't that accurate though, so just use 500us here
            unnecessaryLatency += std::max(journal.result() + 500us - *renderTime, 0ns);
            totalRenderTime += *renderTime;
            journal.add(*renderTime, stats.actualPageflipTimestamp);
        }
    }

    // for median calculation
    std::erase_if(frames, [](const auto &val) {
        return val.renderEnd == 0ns && val.renderStart == 0ns;
    });
    std::ranges::sort(frames, [](const auto &left, const auto &right) {
        return left.renderEnd - left.renderStart < right.renderEnd - right.renderStart;
    });
    const auto medianRenderTime = frames[frames.size() / 2].renderEnd - frames[frames.size() / 2].renderStart;

    std::println("frame stats from {}:", file.string());
    std::println("- total of {} frames", frames.size());
    std::println("- dropped {} frames ({:.2}%)", dropped, 100 * dropped / double(frames.size()));
    std::println("    - {} because of too late commits ({:.2}%)", cpuDropped, 100 * cpuDropped / double(frames.size()));
    if (hasRenderTimeEstimation) {
        std::println("    - {} because of late rendering start ({:.2}%)", lateStartDropped, 100 * lateStartDropped / double(frames.size()));
    }
    std::println("    - {} because of render times ({:.2}%)", gpuDropped, 100 * gpuDropped / double(frames.size()));
    std::println("- {} average render time", std::chrono::duration_cast<std::chrono::microseconds>(totalRenderTime / frames.size()));
    std::println("- {} median render time", std::chrono::duration_cast<std::chrono::microseconds>(medianRenderTime));
    std::println("- {} total 'unnecessary' latency", std::chrono::duration_cast<std::chrono::seconds>(unnecessaryLatency));
    std::println("- would've estimated render times too low in {} frames ({:.2}%)", estimatedTooLittle, 100 * estimatedTooLittle / double(frames.size()));
    std::println("- would've estimated 50+% too much render time in {} frames ({:.2}%)", estimatedTooMuch, 100 * estimatedTooMuch / double(frames.size()));
    std::println();
}

int main()
{
    for (const auto &file : std::filesystem::directory_iterator(getenv("HOME"))) {
        if (!file.is_regular_file() || file.path().extension() != ".csv" || !file.path().string().contains("kwin perf statistics")) {
            continue;
        }
        analyze(file);
    }
    std::println("done");
}
